#! /usr/bin/make -f
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Mitsutoshi Nakano <ItSANgo@gmail.com>
bin_PROGRAMS=style
style_SRCS=style.c
# @see https://matt.sh/howto-c
CFLAGS=-Wall -Wextra -Wshadow -g
SRCS=$(st5yle_SRCS)
style_OBJS=$(style_SRCS:.c=.o)
OBJS=$(style_OBJS)
DEPS=$(OBJS:.o=.d)
TARGETS=$(bin_PROGRAMS)

.PHONY: all check clean
all: $(TARGETS)
style: $(style_OBJS)
	$(CC) -o $@ $(OBJS)
# @see https://qiita.com/shu_ohm1/items/6481c54df9162abb0aa1
.c.o:
	$(CC) -M $(CPPFLAGS) $< >$*.d
	$(COMPILE.c) $(OUTPUT_OPTION) $<
check: $(TARGETS)
	./style
clean:
	$(RM) $(TARGETS) $(OBJS) $(DEPS)

ifeq ($(findstring clean,${MAKECMDGOALS}),)
  -include ${DEPS}
endif
