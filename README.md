# README.md - Mitsutoshi' s favorite C style

## Description

Mitsutoshi's favorite C style has a similar with Linux Kernel standard excepted
AlwaysBreakAfterReturnType.

## Copyright

Mitsutoshi Nakano <ItSANgo@gmail.com>

## License

SPDX-License-Identifier: MIT
