/**
 * @file style.c
 * @author Mitsutoshi Nakano (bkbin005@rinku.zaq.ne.jp)
 * @brief C11 style sample.
 * @version 0.1
 * @date 2021-01-09
 *
 * SPDX-License-Identifier: MIT
 * Copyright (c) 2021 Mitsutoshi Nakano <ItSANgo@gmail.com>
 *
 */
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

/**
 * @brief C11 style sample.
 *
 * @param argc was not used.
 * @param argv was not used.
 * @return is always 0.
 * @see https://qiita.com/raccy/items/8fd7765d1d22f29e5d82
 */
int
main(int argc, char *argv[])
{
	for (int64_t i = 0; i < 10; ++i) {
		printf("%" PRId64 "\n", i);
	}
	return 0;
}
